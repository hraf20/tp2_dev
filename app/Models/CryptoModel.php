<?php namespace Models;

class CryptoModel {
  private $contents;
  private $contentsDecoded;

  public function getCrypto() {
    
    
    $contents = file_get_contents('../data.json');
    $contentsDecoded = json_decode($contents, true);

  return $contentsDecoded;
  }

  public function get_details_crypto($nom){

    $contents = file_get_contents('../data.json');
    $contentsDecoded = json_decode($contents, true);
    $crypto=[];
    
    foreach($contentsDecoded as $content){
      if($content['name']==$nom){
        $crypto[]=$content;
      }
    }
    return $crypto;
  }
}
