<?php namespace Controllers;

use Models\CryptoModel;
use Util\View;

class CryptoController{
    public function listCrypto(){
        $model = new CryptoModel();
        $contentsDecoded=$model->getCrypto();
        
        $view = new View();
        $path = 'crypto/index';
        echo $view->render($path, compact('contentsDecoded'));
        
    }

    public function details_crypto($nom){
        $model = new CryptoModel();
        $crypto=$model->get_details_crypto($nom);

        $view=new View();
        $path = 'crypto/details_crypto';
        echo $view->render($path, compact('crypto'));

    }
}