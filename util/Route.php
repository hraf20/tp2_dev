<?php namespace Util;

use Util\View;
use Configuration\Config;
class Route
{
    public function __construct($args)
    {
        
        $url=explode("/",$_SERVER['REQUEST_URI']);
        $nbParam = count($url);

        $namespace ="Controllers";
        if(count($args)>6){
            $controller = $args[$nbParam-3];
            $method = $args[$nbParam-2];
            $attribut=$args[$nbParam-1];
        }else{
            $controller = $args["class"];
            $method = $args["function"];
            $attribut=null;
        }
        $class = $namespace."\\".$controller;

        if (! class_exists($class)) {
            return $this->not_found();
        }

        if (! method_exists($class, $method)) {
            return $this->not_found();
        }

        $classInstance = new $class;

        call_user_func_array(array($classInstance, $method), $args=[$attribut]);
    }

    public function not_found()
    {
        $view = new View();
        return $view->render('404');
    }
}